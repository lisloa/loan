<div class="row">
    
		    <section class="content">  
	        <div class="box box-success">
            <div class="box-body">
              <div class="table-responsive">
             <div class="box-body">
<form method="post">
			 <a href="dashboard.php?id=<?php echo $_SESSION['tid']; ?>&&mid=<?php echo base64_encode("401"); ?>"><button type="button" class="btn btn-flat btn-warning"><i class="fa fa-mail-reply-all"></i>&nbsp;Atrás</button> </a> 
	 <button type="submit" class="btn btn-flat btn-danger" name="delete"><i class="fa fa-times"></i>&nbsp;Eliminar</button>
	<a href="newpayments.php?id=<?php echo $_SESSION['tid']; ?>&&mid=<?php echo base64_encode("408"); ?>"><button type="button" class="btn btn-flat btn-info"><i class="fa fa-dollar"></i>&nbsp;Nuevo pago</button></a>
	
	<a href="printpayment.php" target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-print"></i>&nbsp;Imprimir pagos</a>
	<a href="excelpayment.php" target="_blank" class="btn btn-success btn-flat"><i class="fa fa-send"></i>&nbsp;Exportar Excel</a>
	
	<hr>		
			  
			 <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><input type="checkbox" id="select_all"/></th>
                  <th>ID</th>
                  <th>Cliente</th>
				  <th>Préstamo</th>
                  <th>Balance</th>
				  <th>Monto a pagar</th>
                  <th>Fecha</th>
				  <th>Cajero</th>
                  <th>Acciones</th>
                 </tr>
                </thead>
                <tbody>
<?php
$tid = $_SESSION['tid'];
$select = mysqli_query($link, "SELECT * FROM payments WHERE tid = '$tid'") or die (mysqli_error($link));
if(mysqli_num_rows($select)==0)
{
echo "<div class='alert alert-info'>¡No se encontraron datos. ¡Vuelve más tarde!</div>";
}
else{
while($row = mysqli_fetch_array($select))
{
$id = $row['id'];
$customer = $row['customer'];
$getin = mysqli_query($link, "SELECT fname, lname, account FROM borrowers WHERE id = '$customer'") or die (mysqli_error($link));
$have = mysqli_fetch_array($getin);
$nameit = $have['fname'].'&nbsp;'.$have['lname'];
//$accte = $have['account'];
$loan = $row['loan'];
$amount_to_pay = $row['amount_to_pay'];
$pay_date = $row['pay_date'];
$select1 = mysqli_query($link, "SELECT * FROM systemset") or die (mysqli_error($link));
while($row1 = mysqli_fetch_array($select1))
{
$balance = $loan - $amount_to_pay;
$currency = $row1['currency']; 
?>    
                <tr>
                <td><input id="optionsCheckbox" class="checkbox" name="selector[]" type="checkbox" value="<?php echo $id; ?>"></td>
                <td><?php echo $id; ?></td>
				<td><?php echo $nameit; ?></td>
				<td><?php echo 'Flexible('.$currency.number_format($loan,2,".",",").')'; ?></td>
                <td><?php echo $currency.number_format($balance,2,".",","); ?></td>
				<td><?php echo $currency.number_format($amount_to_pay,2,".",","); ?></td>
				<td><?php echo $pay_date; ?></td>
				<td><?php echo $name; ?></td>
                <td><a href="view_pmt.php?id=<?php echo $id;?>"><button type="button" class="btn btn-flat btn-info"><i class="fa fa-eye"></i>&nbsp;Ver</button></a></td>		    
			    </tr>
<?php } } } ?>
             </tbody>
                </table>  
			
<?php
						if(isset($_POST['delete'])){
						$idm = $_GET['id'];
							$id=$_POST['selector'];
							$N = count($id);
						if($id == ''){
						echo "<script>alert('Fila no seleccionada para ser eliminarda'); </script>";	
						echo "<script>window.location='listpayment.php?id=".$_SESSION['tid']."&&mid=".base64_encode("408")."'; </script>";
							}
							else{
							for($i=0; $i < $N; $i++)
							{
								$result = mysqli_query($link,"DELETE FROM payments WHERE id ='$id[$i]'");
								echo "<script>alert('Datos eliminados con éxito'); </script>";
								echo "<script>window.location='listpayment.php?id=".$_SESSION['tid']."&&mid=".base64_encode("408")."'; </script>";
							}
							}
							}
?>			
				
</form>
                </div>

				</div>	
				</div>
			
</div>	
			
			<div class="box box-info">
            <div class="box-body">
            <div class="alert alert-info" align="center" class="style2" style="color: #FFFFFF">NÚMERO DE SOLICITANTES DE PRÉSTAMO:&nbsp;
			<?php 
			$call3 = mysqli_query($link, "SELECT * FROM payments ");
			$num3 = mysqli_num_rows($call3);
			?>
			<?php echo $num3; ?> 
			
			</div>
			
			 <div id="chartdiv1"></div>								
			</div>
			</div>		
       
</div>