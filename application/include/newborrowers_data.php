<div class="box">
	       <div class="box-body">
			<div class="panel panel-success">
            <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-user"></i> Nuevo prestatario</h3>
            </div>
             <div class="box-body">
            
			 <form class="form-horizontal" method="post" enctype="multipart/form-data">
			  <?php echo '<div class="alert alert-info fade in" >
			  <a href = "#" class = "close" data-dismiss= "alert"> &times;</a>
  				<strong>Tenga en cuenta que&nbsp;</strong> &nbsp;&nbsp;Algunos campos son obligatorios.
				</div>'?>
             <div class="box-body">
<?php
if(isset($_POST['save']))
{
$fname =  mysqli_real_escape_string($link, $_POST['fname']);
$lname = mysqli_real_escape_string($link, $_POST['lname']);
$email = mysqli_real_escape_string($link, $_POST['email']);
$phone = mysqli_real_escape_string($link, $_POST['phone']);
$addrs1 = mysqli_real_escape_string($link, $_POST['addrs1']);
//$addrs2 = mysqli_real_escape_string($link, $_POST['addrs2']);
$city = mysqli_real_escape_string($link, $_POST['city']);
$state = mysqli_real_escape_string($link, $_POST['state']);
$zip = mysqli_real_escape_string($link, $_POST['zip']);
$country = mysqli_real_escape_string($link, $_POST['country']);
$comment = mysqli_real_escape_string($link, $_POST['comment']);
$account = mysqli_real_escape_string($link, $_POST['account']);
$rnc = mysqli_real_escape_string($link, $_POST['rnc']);
$timebusinness = mysqli_real_escape_string($link, $_POST['timebusinness']);
$status = "Pending";

//$image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
//$image_name = addslashes($_FILES['image']['name']);
//$image_size = getimagesize($_FILES['image']['tmp_name']);

$target_dir = "../img/";
$target_file = $target_dir.basename($_FILES["image"]["name"]);
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$check = getimagesize($_FILES["image"]["tmp_name"]);

if($check == false)
{
	echo '<meta http-equiv="refresh" content="2;url=view_emp.php?tid='.$id.'&&mid='.base64_encode("409").'">';
	echo '<br>';
	echo'<span class="itext" style="color: #FF0000">Tipo de archivo invalido</span>';
}
elseif(file_exists($target_file)) 
{
	echo '<meta http-equiv="refresh" content="2;url=view_emp.php?tid='.$id.'&&mid='.base64_encode("409").'">';
	echo '<br>';
	echo'<span class="itext" style="color: #FF0000">El registro ya existe.</span>';
}
elseif($_FILES["image"]["size"] > 500000)
{
	echo '<meta http-equiv="refresh" content="2;url=view_emp.php?tid='.$id.'&&mid='.base64_encode("409").'">';
	echo '<br>';
	echo'<span class="itext" style="color: #FF0000">¡La imagen no debe superar los 500 KB!</span>';
}
elseif($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif")
{
	echo '<meta http-equiv="refresh" content="2;url=view_emp.php?tid='.$id.'&&mid='.base64_encode("409").'">';
	echo '<br>';
	echo'<span class="itext" style="color: #FF0000">Lo sentimos, solo se permiten archivos JPG, JPEG, PNG y GIF.</span>';
}
else{
	$sourcepath = $_FILES["image"]["tmp_name"];
	$targetpath = "../img/" . $_FILES["image"]["name"];
	move_uploaded_file($sourcepath,$targetpath);
	
	$location = "img/".$_FILES['image']['name'];

$insert = mysqli_query($link, "INSERT INTO borrowers VALUES('','$fname','$lname','$email','$phone','$addrs1','$city','$state','$zip','$country','$comment','$account','0.0','$location',NOW(),'$status')") or die (mysqli_error($link));
if(!$insert)
{
echo "<div class='alert alert-info'>No se pueden insertar registros del prestatario. Por favor intente nuevamente.</div>";
}
else{
echo "<div class='alert alert-success'>¡Información del prestatario creada con éxito!</div>";
}
}
}
?>			  				
			<div class="form-group">
            <label for="" class="col-sm-2 control-label">Imagen</label>
			<div class="col-sm-10">
  		  			 <input type='file' name="image" onChange="readURL(this);" /required>
       				 <img id="blah"  src="../avtar/user2.png" alt="Image Here" height="100" width="100"/>
			</div>
			</div>
			
			<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Cédula</label>
                  <div class="col-sm-10">
                  <input name="account" type="text" class="form-control" placeholder="Cédula">
                  </div>
                  </div>
				  
			<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Nombre</label>
                  <div class="col-sm-10">
                  <input name="fname" type="text" class="form-control" placeholder="Nombre" required>
                  </div>
                  </div>
				  
		<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Apellido</label>
                  <div class="col-sm-10">
                  <input name="lname" type="text" class="form-control" placeholder="Apellido" required>
                  </div>
                  </div>
				  
		<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Email</label>
                  <div class="col-sm-10">
                  <input type="email" name="email" type="text" class="form-control" placeholder="Email">
                  </div>
                  </div>
				  
		<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Teléfono</label>
                  <div class="col-sm-10">
                  <input name="phone" type="text" class="form-control" placeholder="Teléfono" required>
                  </div>
                  </div>
				  
				  
		 <div class="form-group">
                  	<label for="" class="col-sm-2 control-label" style="color:#009900">Dirección</label>
                  	<div class="col-sm-10"><textarea name="addrs1"  class="form-control" rows="4" cols="80"></textarea></div>
          </div>
					
		<!--	<div class="form-group">
                  	<label for="" class="col-sm-2 control-label" style="color:#009900">Dirección 2</label>
                  	<div class="col-sm-10"><textarea name="addrs2"  class="form-control" rows="4" cols="80"></textarea></div>
          	</div>-->
			
			
			<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Ciudad</label>
                  <div class="col-sm-10">
                  <input name="city" type="text" class="form-control" placeholder="Ciudad"required >
                  </div>
                  </div>
				  
		<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Provincia</label>
                  <div class="col-sm-10">
                  <input name="state" type="text" class="form-control" placeholder="Provincia" required>
                  </div>
                  </div>
				  
				  <div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Código postal</label>
                  <div class="col-sm-10">
                  <input name="zip" type="text" class="form-control" placeholder="Código postal" >
                  </div>
                  </div>
				  
		<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">País</label>
                  <div class="col-sm-10">
                  <input name="country" type="text" class="form-control" value="Dominican Republic" readonly>
				            
									 </div>
                 					 </div>

                 
									 
									 
				<div class="form-group">
                  	<label for="" class="col-sm-2 control-label" style="color:#009900">Comentario</label>
                  	<div class="col-sm-10"><textarea name="comment"  class="form-control" rows="4" cols="80"></textarea></div>
          	</div>

          	 <div class="form-group">
		              <label for="" class="col-sm-12 control-label title-label">Información de la empresa</label>
		              
		            </div>  
		            <hr>

		     <div class="form-group">
                 <label for="" class="col-sm-2 control-label" style="color:#009900">Nombre de la empresa</label>
                  	<div class="col-sm-10">

                  		<select name="rnc" class="customer select form-control" style="width: 100%;">
							<option selected="selected">--Seleccione la empresa--</option>
								<?php
									$get = mysqli_query($link, "SELECT * FROM empresa") or die (mysqli_error($link));
									while($rows = mysqli_fetch_array($get))
										{
											echo '<option value="'.$rows['rnc'].'">'.$rows['empresa_nombre'].'</option>';
										}
								?>
			            </select>


                  	</div>
          	</div>

          	<div class="form-group">
                  <label for="" class="col-sm-2 control-label" style="color:#009900">Tiempo en la empresa</label>
                  <div class="col-sm-10">
                  <input name="timebusinness" type="text" class="form-control">
				            
			 </div>
				 </div>





			 </div>
			 
			  <div align="right">
              <div class="box-footer">
                			<!--	<button type="reset" class="btn btn-primary btn-flat"><i class="fa fa-times">&nbsp;Resert</i></button>-->
                				<button name="save" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save">&nbsp;Guardar</i></button>

              </div>
			  </div>

			 </form> 


</div>	
</div>	
</div>
</div>