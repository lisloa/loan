
<!-- NUEVAS EMPRESAS -->

<div class="box">
 <div class="box-body">
		<div class="panel panel-success">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-user"></i> Nueva Empresa</h3>
      </div>

      <div class="box-body">
            
		    <form class="form-horizontal" method="post" enctype="multipart/form-data" >
			  <?php echo '<div class="alert alert-info fade in" >
			  <a href = "#" class = "close" data-dismiss= "alert"> &times;</a>
  				<strong>Tenga en cuenta que&nbsp;</strong> &nbsp;&nbsp;Algunos campos son obligatorios.
				</div>'?>
          
          <div class="box-body">
<?php
if(isset($_POST['gaurdarempresa']))
{
  //Datos de la empresa
$tid = $_SESSION['tid'];
$rnc =  mysqli_real_escape_string($link, $_POST['rnc']);
$empresa_nombre = mysqli_real_escape_string($link, $_POST['empresa_nombre']);
$empresa_direccion = mysqli_real_escape_string($link, $_POST['empresa_direccion']);
$empresa_telefono = mysqli_real_escape_string($link, $_POST['empresa_telefono']);
$empresa_correo = mysqli_real_escape_string($link, $_POST['empresa_correo']);
$empresa_comentario = mysqli_real_escape_string($link, $_POST['empresa_comentario']);
// $empresa_rm = mysqli_real_escape_string($link, $_POST['empresa_rm']);
// $empresa_doc_representante = mysqli_real_escape_string($link, $_POST['empresa_doc_representante']);
// $empresa_contrato = mysqli_real_escape_string($link, $_POST['empresa_contrato']);


//$image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
//$image_name = addslashes($_FILES['image']['name']);
//$image_size = getimagesize($_FILES['image']['tmp_name']);


// $target_dir = "../img/";
$target_file = ".../img/registromercantil/".basename($_FILES["empresa_rm"]["name"]);
$target_file2 = ".../img/documentorepresentante/".basename($_FILES["empresa_doc_representante"]["name"]);
$target_file3 = ".../img/contrato/".basename($_FILES["empresa_contrato"]["name"]);
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$imageFileType2 = pathinfo($target_file2,PATHINFO_EXTENSION);
$imageFileType3 = pathinfo($target_file3,PATHINFO_EXTENSION);
$rm = getimagesize($_FILES["empresa_rm"]["tmp_name"]);
$doc_representante = getimagesize($_FILES["empresa_doc_representante"]["tmp_name"]);
$contrato = getimagesize($_FILES["empresa_contrato"]["tmp_name"]);

// if(($rm || $doc_representante || $contrato ) == false)
// {
//   echo '<meta http-equiv="refresh" content="2;url=newbuaainwa_data?tid='.$id.'&&mid='.base64_encode("409").'">';
//   echo '<br>';
//   echo'<span class="itext" style="color: #FF0000">Tipo de archivo invalido</span>';
// }
// else
// if(file_exists($target_file)) 
// {
//   echo '<meta http-equiv="refresh" content="2;url=newbuaainwa_data?tid='.$id.'&&mid='.base64_encode("409").'">';
//   echo '<br>';
//   echo'<span class="itext" style="color: #FF0000">El registro ya existe.</span>';
// }
// else
if(($_FILES["empresa_rm"]["size"]  || $_FILES["empresa_doc_representante"]["size"]  || $_FILES["empresa_contrato"]["size"] ) > 500000)
{
  echo '<meta http-equiv="refresh" content="2;url=newbuaainwa_data?tid='.$id.'&&mid='.base64_encode("409").'">';
  echo '<br>';
  echo'<script>alert("¡La imagen no debe superar los 500 KB!"); </script>';
  echo "<script>window.location='newbussines_data.php'; </script>";
}
elseif($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf" && $imageFileType != "docx" && $imageFileType2 != "jpg" && $imageFileType2 != "png" && $imageFileType2 != "jpeg" && $imageFileType2 != "pdf" && $imageFileType2 != "docx" && $imageFileType3 != "jpg" && $imageFileType3 != "png" && $imageFileType3 != "jpeg" && $imageFileType3 != "pdf" && $imageFileType3 != "docx")
{
  echo '<meta http-equiv="refresh" content="2;url=newbuaainwa_data?tid='.$id.'&&mid='.base64_encode("409").'">';
  echo '<br>';
  echo'<script>alert("Lo sentimos, solo se permiten archivos JPG, JPEG, PNG, PDF y WORD."); </script>';
  echo "<script>window.location='newbussines_data.php'; </script>";
}
else{
  $sourcepath = $_FILES["empresa_rm"]["tmp_name"];
  $sourcepath2 = $_FILES["empresa_doc_representante"]["tmp_name"];
  $sourcepath3 = $_FILES["empresa_contrato"]["tmp_name"];
  $targetpath = "../img/registromercantil/". $_FILES["empresa_rm"]["name"];
  $targetpath2 = "../img/documentorepresentante/". $_FILES["empresa_doc_representante"]["name"];
  $targetpath3 = "../img/contrato/". $_FILES["empresa_contrato"]["name"];
  move_uploaded_file($sourcepath,$targetpath);
  move_uploaded_file($sourcepath2,$targetpath2);
  move_uploaded_file($sourcepath3,$targetpath3);
  
  $location = "img/registromercantil/".$_FILES['empresa_rm']['name'];
  $location1 = "img/documentorepresentante/".$_FILES['empresa_doc_representante']['name'];
  $location2 = "img/contrato/".$_FILES['empresa_contrato']['name'];

$reply = mysqli_query($link,"SELECT * FROM empresa WHERE rnc = '$rnc'");if ($reply->num_rows > 0){

   echo "<script>alert('La empresa ingresada ya existe!'); </script>";
    echo "<script>window.location='newbussines_data.php'; </script>";

} else {

$insert = mysqli_query($link, "INSERT INTO empresa VALUES('$rnc','$empresa_nombre','$empresa_direccion','$empresa_telefono','$empresa_correo','$empresa_comentario' ,'$location', '$location1', '$location2')");
if(!$insert)
{
echo "<div class='alert alert-info'>No se puede insertar el registro de la empresa. Por favor intente nuevamente.</div>";
}
else{
echo "<script>alert('Nueva empresa creada correctamente!'); </script>";
echo "<script>window.location='dashboard.php'; </script>";
}
}
//Datos del encargado de RRHH
$id_rrhh = mysqli_real_escape_string($link, $_POST['id_rrhh']);
$nombre_completo_rrhh = mysqli_real_escape_string($link, $_POST['nombre_completo_rrhh']);
$email_rrhh = mysqli_real_escape_string($link, $_POST['email_rrhh']);

$insert2 = mysqli_query($link, "INSERT INTO in_charge_rrhh VALUES('$id_rrhh','$nombre_completo_rrhh','$email_rrhh','$rnc')") or die (mysqli_error($link));


//Datos del encargado de Contabilidad
$id_cont = mysqli_real_escape_string($link, $_POST['id_cont']);
$nombre_completo_cont = mysqli_real_escape_string($link, $_POST['nombre_completo_cont']);
$email_cont = mysqli_real_escape_string($link, $_POST['email_cont']);

$insert3 = mysqli_query($link, "INSERT INTO accounting_officer VALUES('$id_cont','$nombre_completo_cont','$email_cont','$rnc')") or die (mysqli_error($link));
}
}

?>			 

            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">RNC</label>
              <div class="col-sm-10">
                <input name="rnc" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" placeholder="SOLO NÙMEROS" maxlength="9"  required>
              </div>
            </div> 				
            		
            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Nombre de la empresa</label>
              <div class="col-sm-10">
                <input name="empresa_nombre" type="text" class="form-control" placeholder="Nombre de la empresa" required>
              </div>
            </div>			

            <div class="form-group">
            	<label for="" class="col-sm-2 control-label" style="color:#009900">Dirección</label>
            	<div class="col-sm-10">
                <textarea name="empresa_direccion" class="form-control" rows="4" cols="80"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Teléfono</label>
              <div class="col-sm-10">
                <input name="empresa_telefono" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" placeholder="Teléfono" maxlength="10" required>
              </div>
            </div>
            				  
            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Email</label>
              <div class="col-sm-10">
                <input type="email" name="empresa_correo" type="text" class="form-control" placeholder="Email">
              </div>
            </div>

            				  			 
            <div class="form-group">
            	<label for="" class="col-sm-2 control-label" style="color:#009900">Comentario</label>
            	<div class="col-sm-10">
                <textarea name="empresa_comentario"  class="form-control" rows="4" cols="80"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label for="" class="col-sm-12 control-label title-label" style="color:#009900">Encargado de RRHH</label>
              
            </div>  
            <hr>


            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Nro. de Cédula</label>
              <div class="col-sm-10">
                <input name="id_rrhh" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" placeholder="Nro. de Cédula" required>
              </div>
            </div> 

            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Nombre Completo</label>
              <div class="col-sm-10">
                <input name="nombre_completo_rrhh" type="text" class="form-control" placeholder="Nombre Completo" required>
              </div>
            </div>   

            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Correo electrónico</label>
              <div class="col-sm-10">
                <input name="email_rrhh" type="text" class="form-control" placeholder="Correo electrónico" required>
              </div>
            </div>  


            <div class="form-group">
              <label for="" class="col-sm-12 control-label title-label" style="color:#009900">Encargado de Contabilidad</label>
              
            </div>  
            <hr>

            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Nro. de Cédula</label>
              <div class="col-sm-10">
                <input name="id_cont" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" placeholder="Nro. de Cédula" required>
              </div>
            </div> 

            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Nombre Completo</label>
              <div class="col-sm-10">
                <input name="nombre_completo_cont" type="text" class="form-control" placeholder="Nombre Completo" required>
              </div>
            </div>   

            <div class="form-group">
              <label for="" class="col-sm-2 control-label" style="color:#009900">Correo electrónico</label>
              <div class="col-sm-10">
                <input name="email_cont" type="text" class="form-control" placeholder="Correo electrónico" required>
              </div>
            </div>  

            <div class="form-group">
              <label for="" class="col-sm-12 control-label title-label" style="color:#009900">Adjunte los siguientes documentos</label>
              
            </div>  
            <hr>

            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Copia de Registro Mercantil</label>
              <div class="col-sm-10">
                <input type='file' name="empresa_rm" onChange="imgChange(this, 'blah22', 'empresa_rm');" required>
                <img id="blah22"  src="../avtar/document.png" alt="Image Here" height="100" width="100"/>
              </div>
            </div>

            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Copia de Cédula de Persona que firma</label>
              <div class="col-sm-10">
                <input type='file' name="empresa_doc_representante" onChange="imgChange(this, 'blah2', 'empresa_doc_representante');" required>
                <img id="blah2"  src="../avtar/document.png" alt="Image Here" height="100" width="100"/>
              </div>
            </div>

            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Copia De contrato Firmado y Notarizado</label>
              <div class="col-sm-10">
                <input type='file' name="empresa_contrato" onChange="imgChange(this, 'blah', 'empresa_contrato');" required>
                <img id="blah"  src="../avtar/document.png" alt="Image Here" height="100" width="100"/>
              </div>
            </div>

          </div>
            			 
  			  <div align="right">
            <div class="box-footer">
              <button name="gaurdarempresa" type="submit" class="btn btn-success btn-flat"><i class="fa fa-save">&nbsp;Guardar</i></button>
            </div>
  			  </div>

			  </form> 
      </div>	
    </div>	
  </div>
</div>
</div>

<script type="text/javascript">
        function imgChange(input, ide='', name='') {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('input[name='+name+']')
                    .next('img')
                    .attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script> 
