<!-- LISTA DE EMPRESAS -->

<div class="row">       
		    <section class="content">  
	        <div class="box box-success">
            <div class="box-body">
              <div class="table-responsive">
             <div class="box-body">
<form method="post" >
			 <a href="dashboard.php?id=<?php echo $_SESSION['tid']; ?>&&mid=<?php echo base64_encode("401"); ?>"><button type="button" class="btn btn-flat btn-warning"><i class="fa fa-mail-reply-all"></i>&nbsp;Atrás</button> </a> 

<!-- BOTON ELIMINAR EMPRESAS -->
<button type="submit" class="btn btn-flat btn-danger" name="borrar" onclick="reload()"><i class="fa fa-times"></i>&nbsp;Eliminar múltiples</button>
<?php
	if (isset($_POST['borrar'])) 
	{
		if (empty($_POST['eliminar']))
		 {
			echo"<script>alert('Selecciona una empresa'); </script>";	
		}
		else 
		{
			foreach ($_POST['eliminar'] as $id_borrar)
			{
				$borrarEmpresa = mysqli_query($link, "DELETE FROM empresa WHERE rnc = $id_borrar");
				// $borrarEmpresa = mysqli_query($link, "DELETE FROM in_charge_rrhh WHERE rnc = $id_borrar");
				// $borrarEmpresa = mysqli_query($link, "DELETE FROM accounting_officer WHERE rnc = $id_borrar");
				// $borrarEmpresa = mysqli_query($link, "DELETE empresa, in_charge_rrhh, accounting_officer INNER JOIN in_charge_rrhh on empresa.rnc=in_charge_rrhh.rnc join accounting_officer on empresa.rnc=accounting_officer.rnc WHERE rnc = $id_borrar");
				echo"<script>alert('Empresa eliminada correctamente'); </script>";
				echo "<script>window.location='dashboard.php?";
			}
		}
	}
?>
	
	<a href="printloan.php" target="_blank" class="btn btn-info btn-flat"><i class="fa fa-print"></i>&nbsp;Imprimir</a>
	<!-- <button onclick="exportTableToExcel('tablaEmpresas', 'listadoEmpresas')">Exportar Excel</button> -->


	<a href="exportbusiness.php" target="_blank" class="btn btn-success btn-flat"><i class="fa fa-send"></i>&nbsp;Exportar Excel</a>
	
	<hr>		
			  
			 <table id="tablaEmpresas" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><input type="checkbox" id="select_all"/></th>
                  <th>RNC</th>
				  <th>Empresa</th>
                  <th>Dirección</th>
                  <th>Teléfono</th>
                  <th>Correo</th>
                  <th>Encargado RRHH </th>
				  <th>Email RRHH</th>
                  <th>Encargado Contabilidad</th>
                  <th>Email Cont</th>
<!--                   <th>Empleados con préstamos</th> -->
<!--                  <th>Empresa</th> -->
               <!--    <th>Estado de aprobación</th>
				  <th>Estado de actualización</th>
                  <th>Acción</th>-->
                 </tr>
                </thead>
                <tbody> 



<?php
//Selecciono Datos de Empresa

	$select = mysqli_query($link, "SELECT empresa.rnc, empresa.empresa_nombre, empresa.empresa_direccion, empresa.empresa_telefono, empresa.empresa_correo, in_charge_rrhh.nombre_completo_rrhh, in_charge_rrhh.email_rrhh, accounting_officer.nombre_completo_cont, accounting_officer.email_cont FROM empresa 
		INNER JOIN in_charge_rrhh ON empresa.rnc = in_charge_rrhh.rnc  JOIN accounting_officer ON empresa.rnc = accounting_officer.rnc") or die (mysqli_error($link));

	if(mysqli_num_rows($select)==0)
		{
			echo "<div class='alert alert-info'>¡Aún no se encontraron datos! ... ¡Vuelve más tarde!</div>";
		}
	else{
		while($row = mysqli_fetch_array($select))
		{
			$rnc = $row['rnc'];
			$empresa_nombre = $row['empresa_nombre'];
			$empresa_direccion = $row['empresa_direccion'];
			$empresa_telefono = $row['empresa_telefono'];
			$empresa_correo = $row['empresa_correo'];
			$nombre_completo_rrhh = $row['nombre_completo_rrhh'];
			$email_rrhh = $row['email_rrhh'];
			$nombre_completo_cont = $row['nombre_completo_cont'];
			$email_cont = $row['email_cont'];

	?>  
                <tr>
             
				<td><input id="optionsCheckbox" class="checkbox" name="eliminar[]" type="checkbox" value="<?php echo $row['rnc'] ?>"></td>
                <td><?php echo $rnc; ?></td>
				<td><?php echo $empresa_nombre; ?></td>
				<td><?php echo $empresa_direccion; ?></td>
                <td><?php echo $empresa_telefono; ?></td>
				<td><?php echo $empresa_correo; ?></td>
				<td><?php echo $nombre_completo_rrhh; ?></td>
				<td><?php echo $email_rrhh; ?></td>
			    <td><?php echo $nombre_completo_cont; ?></td>
				<td><?php echo $email_cont; ?></td>
				</tr>
<!-- 				<td>1</td>	
				<td><a href="#">Ver empresa</td> -->

		 <?php } ?>				

<?php } ?>
             </tbody>
                </table>  
		

</form>				

              </div>


	
</div>	
</div>
</div>	
</div>